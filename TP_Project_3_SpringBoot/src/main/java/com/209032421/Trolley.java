package com.209032421;


@Entity
public class Trolley extends Car implements Serializable{
    public Trolley(String cMake, String cModel, int cYear)
    {
        super(cMake, cModel, cYear);
    }

    public String engine(){
        return "squeak squeak in embarrassment";
    }

}
