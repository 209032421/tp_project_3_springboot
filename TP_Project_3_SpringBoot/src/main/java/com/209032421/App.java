package com.209032421;

/**
 * Hello world!
 *
 */
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.*;

@SpringBootApplication
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        SpringApplication.run(App.class, args);


//        List<CarInterface> carList = new ArrayList();
//        Set<CarInterface> hashSet = new HashSet();
//        Map<String, CarInterface> hashMap = new HashMap();
//
//        Trolley shoprite = new Trolley("Tortuose", "Slowster", 2011);
//        Trolley pnp = new Trolley("slug", "Speedster", 2013);
//        Trolley woolworths = new Trolley("Snail", "Slickster", 2015);
//
//        Toyota corolla = new Toyota("Toyota", "20v", 2011);
//        Toyota conquest = new Toyota("Toyota", "16v", 2013);
//        Toyota camry = new Toyota("Toyota", "2.0", 2015);
//
//        Porsche p = new Porsche("Porsche", "boxtser", 2011);
//        Porsche p2 = new Porsche("Porsche", "911", 2013);
//        Porsche p3= new Porsche("Porsche", "Carrera GT", 2015);
//
//        carList.add(shoprite);
//        carList.add(pnp);
//        carList.add(woolworths);
//
//        carList.add(corolla);
//        carList.add(conquest);
//        carList.add(camry);
//
//        carList.add(p);
//        carList.add(p2);
//        carList.add(p3);
//
//        hashSet.add(shoprite);
//        hashSet.add(pnp);
//        hashSet.add(woolworths);
//
//        hashSet.add(corolla);
//        hashSet.add(conquest);
//        hashSet.add(camry);
//
//        hashSet.add(p);
//        hashSet.add(p2);
//        hashSet.add(p3);
//
//
//        hashMap.put("1", shoprite);
//        hashMap.put("2", corolla);
//        hashMap.put("3", p);
//
//
//        System.out.println("Printing List Contents");
//        System.out.println(carList);
//
//
//
//        System.out.println("Printing Set Contents");
//        System.out.println(hashSet);
//
//        System.out.println("Printing Map Contents");
//        System.out.println(hashMap);
//
//        System.out.println("Printing Interface implementation");
//        for(int i=0; i<carList.size(); i++)
//        {
//            System.out.println(carList.get(i) + "\nengine: " + carList.get(i).engine());
//        }

    }
}
