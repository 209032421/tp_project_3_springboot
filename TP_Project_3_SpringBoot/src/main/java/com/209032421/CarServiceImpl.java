package com.209032421;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class CarServiceImpl implements CarService{
    @Autowired
    private CarRepository carRepository;

    @Override
    public Car create(Car c)
    {
        return carRepository.save(c);
    }

    @Override
    public Set<Car> readAll()
    {
        Iterable<Car> cars = CarRepository.findAll();
        Set carSet = new HashSet();
        for(Car car:cars)
        {
            carSet.add(c);
        }
        return carSet;
    }

    @Override
    public void deleteAll()
    {
        carRepository.deleteAll();
    }
}
