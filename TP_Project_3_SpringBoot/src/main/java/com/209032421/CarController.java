package com.209032421;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping(path="/car")
public class CarController {

    @Autowired
    private CarServiceImpl carService;

    @GetMapping(path="/all")
    public @ResponseBody
    Set<Car> getAllCars()
    {
        return carService.readAll();
    }

    @RequestMapping(value="/add", method = RequestMethod.POST)
    public ResponseEntity addCar(@RequestBody Car car){
        carService.create(car);
        return new ResponseEntity(car, HttpStatus.OK);
    }

    @RequestMapping(value="/deleteAll", method = RequestMethod.DELETE)
    public void deleteAllCars()
    {
        carService.deleteAll();
    }

}