package com.209032421;

import java.util.Optional;
import java.util.Set;

public interface CarService {
    public Car create(Car customer);

    public Set<Car> readAll();

    public void deleteAll();
}